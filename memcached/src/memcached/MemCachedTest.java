package memcached;

import memcached.connect.MemcacheManager;

public class MemCachedTest {  
	  
    public static void main(String[] args){  
        String k = "雅布珊1";  
        String k1 = "雅布珊";  
        String v = "我是发哥2";  
          
        System.out.println("-------------------MemCached set begin--------------");  
        MemcacheManager.set(k, v); 
        MemcacheManager.set(k, v, 1);
        System.out.println("setValue:" + v);  
        System.out.println("-------------------MemCached set end--------------");  
          
        System.out.println("-------------------MemCached get begin--------------");  
        String r = (String)MemcacheManager.get(k);  
        System.out.println("getValue:" + r);  
        String r1 = (String)MemcacheManager.get(k1);  
        System.out.println("getValue:" + r1);  
       // System.out.println("-------------------MemCached get end--------------");  
    }  
} 