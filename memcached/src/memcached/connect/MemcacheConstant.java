package memcached.connect;

public class MemcacheConstant {
	   //服务器列表和其权重  
   static final String[] MEMCACHED_SERVERS  = {"123.207.59.73:11211"};  
   static final Integer[] MEMCACHED_WEIGHTS  = {3};  
   //设置初始连接数、最小最大连接数、最大处理时间  
   static final int MEMCACHED_INIT_CONN=5;
   static final int MEMCACHED_MIN_CONN=5;
   static final int MEMCACHED_MAX_CONN=250;
   static final long MEMCACHED_MAX_IDLE=1000*60*60*6;
   //设置主线程的睡眠时间
   static final int MEMCACHED_MAIN_SLEEP=30;
  
}
