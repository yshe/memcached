package memcached.connect;

import java.util.Date;

import memcached.test.Person;

import com.danga.MemCached.MemCachedClient;

public class MemcachedSaveOrder {
	//获取Client方法  
    public static MemCachedClient getClient(){  
        return Memcache.getInstance().getClient();  
    } 
    
    /**
     * Memcached set 命令用于将 value(数据值) 存储在指定的 key(键) 中。
     * 如果set的key已经存在，该命令可以更新该key所对应的原来的数据，也就是实现更新的作用
     * @param key 键
     * @param value 值
     * @param exptime 过期时间(秒)
     * @return
     */
    public static Boolean set(String key, Object value ,long exptime){  
		if(exptime>0){
			return getClient().set(key, value, new Date(exptime*1000));
		}else {
			return getClient().set(key, value); 
		}
		
    } 
    
    /**
     * Memcached add 命令用于将 value(数据值) 存储在指定的 key(键) 中。
     * 如果 add 的 key 已经存在，则不会更新数据，之前的值将仍然保持相同，并且您将获得响应Flase。
     * 如果add的key不存在，则新增数据，获得相应true
     * @param key
     * @param value
     * @param exptime过期时间(秒)
     * @return
     */
    public static Boolean add(String key,Object value,long exptime){
    	if(exptime>0){
			return getClient().add(key, value, new Date(exptime*1000));
		}else {
			return getClient().add(key, value); 
		}
    	
    }
    
    /**
     * Memcached replace 命令用于替换已存在的 key(键) 的 value(数据值)。
	 *如果 key 不存在，则替换失败，并且您将获得响应False。
	 *如果 key 不存在,则替换成功，并且你将获得相应true
     * @param key
     * @param value
     * @param exptime 过期时间(秒)
     * @return
     */
    public static Boolean replace(String key,Object value,long exptime){
    	if(exptime>0){
			return getClient().replace(key, value, new Date(exptime*1000));
		}else {
			return getClient().replace(key, value); 
		}
    }
    /**
     * Memcached append 命令用于向已存在 key(键) 的 value(数据值) 后面追加数据 
     * @param key
     * @param value
     * @param exptime
     * @return
     */
    public static Boolean append(String key,Object value,Integer falg){
    	
    		if(falg>0){
    			return getClient().append(key, value, falg);
    		}else {
				return getClient().append(key, value);
			}
    }
    
    /**
     * Memcached prepend 命令用于向已存在 key(键) 的 value(数据值) 前面追加数据 
     * @param key
     * @param value
     * @param falg
     * @return
     */
    public static Boolean prepend (String key,Object value,Integer falg){
    		if(falg>0){
    			return getClient().prepend(key, value, falg);
    		}else {
				return getClient().prepend(key, value);
			}
		
    }
    /**
     *Memcached CAS（Check-And-Set 或 Compare-And-Swap） 命令用于执行一个"检查并设置"的操作
	 *它仅在当前客户端最后一次取值后，该key 对应的值没有被其他客户端修改的情况下， 才能够将值写入。
     *检查是通过cas_token参数进行的， 这个参数是Memcach指定给已经存在的元素的一个唯一的64位值
     * @param key
     * @param value
     * @param exptime过期时间(秒)
     * @return
     */
    public static Boolean cas(String key,String value,long exptime){
    		long casToken=1;
    		casToken=  getClient().gets(key).getCasUnique();
    		if(exptime>0){
    			return  getClient().cas(key,value,new Date(exptime*1000),casToken);
    		}else {
				return getClient().cas(key, value,casToken);
			}
    }
    
    /**
     * Memcached get 命令获取存储在 key(键) 中的 value(数据值) ，如果 key 不存在，则返回空。
     * @param key
     * @return
     */
    public static Object get(String key){  
        return getClient().get(key);  
    }  
    /**
     * Memcached gets 命令获取带有 CAS 令牌存 的 value(数据值) ，如果 key 不存在，则返回空
     * @param key
     * @return
     */
    public static Object gets(String key){  
        return getClient().gets(key);
      
    }  
    /**
     * Memcached delete 命令用于删除已存在的 key(键)。
     * @param key
     * @return
     */
    public static Object delete(String key){  
        return getClient().delete(key);
    }  
    /**
     * 自增
     * Memcached incr 与 decr 命令用于对已存在的 key(键) 的数字值进行自增或自减操作。
     *incr 与 decr 命令操作的数据必须是十进制的32位无符号整数。
	 *如果 key 不存在返回 NOT_FOUND，如果键的值不为数字，则返回 CLIENT_ERROR，其他错误返回 
     * @param key
     * @return exptime 
     */
    public static Object incr(String key,long value,String exptime){  
    	try {
    		Integer TIME =Integer.parseInt(exptime);
    		if(TIME>0){
    			return getClient().decr(key,value,TIME);
    		}else {
				return getClient().decr(key, value);
			}
		} catch (Exception e) {
			return getClient().decr(key, value);
		}
    }  
    /**
     * 自减
     * Memcached incr 与 decr 命令用于对已存在的 key(键) 的数字值进行自增或自减操作。
     *incr 与 decr 命令操作的数据必须是十进制的32位无符号整数。
	 *如果 key 不存在返回 NOT_FOUND，如果键的值不为数字，则返回 CLIENT_ERROR，其他错误返回 
     * @param key
     * @return
     */
    public static Object decr(String key,long value,String exptime){  
    	try {
    		Integer TIME =Integer.parseInt(exptime);
    		if(TIME>0){
    			return getClient().decr(key,value,TIME);
    		}else {
				return getClient().decr(key, value);
			}
		} catch (Exception e) {
			return getClient().decr(key, value);
		}
    } 
    
    public static Boolean flush_all(){
    	return getClient().flushAll();
    	
    }
    
    public static void main(String[] args) {
    	Person person=new Person();
    	person.setAge(11);
    	person.setName("yabushan");
    	//set("fage", person, 4);
    	//System.out.println(get("fage"));
    	//cas("fage", "sdfdf", 10);
    	System.out.println(get("fage"));
    	System.out.println(gets("fage"));
	}
   

}
