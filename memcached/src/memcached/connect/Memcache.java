package memcached.connect;

import com.danga.MemCached.MemCachedClient;  
import com.danga.MemCached.SockIOPool;  
  
public class Memcache {
  
    protected static Memcache mc = new Memcache();  
      
    protected MemCachedClient mcc = new MemCachedClient();  
      
    static{  
        //服务器列表和其权重  
      //  String[] servers = {"123.207.59.73:11211"};  
      //  Integer[] weights = {3};  
        //获取sock连接池的实例对象  
        SockIOPool pool = SockIOPool.getInstance();  
        //设置服务器信息  
        pool.setServers(MemcacheConstant.MEMCACHED_SERVERS);  
        pool.setWeights(MemcacheConstant.MEMCACHED_WEIGHTS);  
        //设置初始连接数、最小最大连接数、最大处理时间  
        pool.setInitConn(MemcacheConstant.MEMCACHED_INIT_CONN);  
        pool.setMinConn(MemcacheConstant.MEMCACHED_MIN_CONN);  
        pool.setMaxConn(MemcacheConstant.MEMCACHED_MAX_CONN);  
        pool.setMaxIdle(MemcacheConstant.MEMCACHED_MAX_IDLE);  
          
        //设置主线程的睡眠时间  
        /*设置连接池维护线程的睡眠时间 
        设置为0，维护线程不启动 
        维护线程主要通过log输出socket的运行状况，监测连接数目及空闲等待时间等参数以控制连接创建和关闭。*/        
        pool.setMaintSleep(MemcacheConstant.MEMCACHED_MAIN_SLEEP);  
  /* Tcp的规则就是在发送一个包之前，本地机器会等待远程主机       
        对上一次发送的包的确认信息到来；这个方法就可以关闭套接字的缓存，       
       以至这个包准备好了就发；    
设置是否使用Nagle算法，因为我们的通讯数据量通常都比较大（相对TCP控制数据）而且要求响应及时，因此该值需要设置为false*/  
        pool.setNagle(false);
        //接建立后对超时的控制
        pool.setSocketTO(3000);  
        pool.setSocketConnectTO(0);  
        pool.setHashingAlg( SockIOPool.CONSISTENT_HASH );  
        //设置Tcp的参数，连接超时等  
        pool.initialize();  
        //压缩设置，超过制定大小（单位K）的数据都会压缩  
        //mcc.setCompressEnable(false);  
        //mcc.setCompressThreshold(64*1024);  
    }  
      
    //保护构造函数，不允许实例化  
    protected Memcache(){}  
      
    public static Memcache getInstance(){  
        return mc;  
    }  
      
    public MemCachedClient getClient(){  
        return mcc;  
    }  
    
    
}  