package memcached.connect;

import java.util.Date;  

import memcached.connect.Memcache;

import com.danga.MemCached.MemCachedClient;  
  
public class MemcacheManager {  
  
    //获取Client方法  
    public static MemCachedClient getClient(){  
        return Memcache.getInstance().getClient();  
    }  
      
    public static Object get(String key){  
        return getClient().get(key);  
    }  
      
    public static boolean set(String key, Object value){  
        return getClient().set(key, value);  
    }  
      
    public static boolean set(String key, Object value, long time){  
         return getClient().set(key, value, new Date(time));  
    }  
      
    public static boolean delete(String key){  
        return getClient().delete(key);  
    }  
}  
